.. _fofrpowerboost:

Matter power spectrum in :math:`f(R)` gravity
---------------------------------------------

e-MANTIS implements an emulator for the non-linear matter power spectrum boost in :math:`f(R)` gravity defined as

.. math::
   B(k)=P_{f(R)}/P_{\Lambda\mathrm{CDM}},

where :math:`P_{f(R)}` is the non-linear matter power spectrum in :math:`f(R)` gravity, and :math:`\Lambda\mathrm{CDM}` is the non-linear matter power spectrum in :math:`\Lambda\mathrm{CDM}`.
In order to obtain the full matter power spectrum in :math:`f(R)` gravity, the prediction from e-MANTIS should be combined with an external non-linear matter power spectrum prediction in :math:`\Lambda\mathrm{CDM}`.

..
   The power spectrum boosts used to train this emulator have been obtained from cosmological N-body simulations covering a wide cosmological parameter space (see :ref:`fofrpowerboost-emulation-range`).
..
   Training data is available for 19 different scale factor nodes.
   For each scale factor node, the training data is decomposed with a Principal Component Analysis (PCA).
   Then, a Gaussian Process Regression (GPR) is used to interpolate each PCA coefficient between the training cosmological models.
   In order to make predictions for any arbitrary scale factor, a linear interpolation is performed between
   the two closest neighbouring nodes.

The emulator is based on cosmological N-body simulations covering a wide cosmological parameter space (see :ref:`fofrpowerboost-emulation-range`).
It has an accuracy better than :math:`3\%` across the whole emulated parameter space for scales :math:`k<7h\mathrm{Mpc}^{-1}` and redshifts :math:`z<2`.
However, this is a conservative bound and in most cases the actual accuracy is better than :math:`1\%`.

More details about the simulations used to obtain the training data, emulation procedure and the estimated accuracy can be found in `The e-MANTIS emulator: fast predictions of the non-linear matter power spectrum in f(R)CDM cosmology <https://doi.org/10.1093/mnras/stad3343>`_.
If you use this emulator, please cite this paper.

.. _fofrpowerboost-emulation-range:

Emulation range
~~~~~~~~~~~~~~~

e-MANTIS is able to predict the matter power spectrum boost in :math:`f(R)` gravity as a function of the present-day background value of the scalaron field :math:`f_{R_0}`, the present-day total matter density parameter :math:`\Omega_m`, the primordial power spectrum normalisation :math:`\sigma_8`, and the scale factor :math:`a_\mathrm{exp}`.

.. warning::

   We follow the standard convention of the field, and by :math:`\sigma_8` we refer to the present-day root-mean-square linear matter fluctuation averaged over a sphere of radius :math:`8h^{-1}\mathrm{Mpc}`, but assuming a :math:`\Lambda\mathrm{CDM}` linear evolution.
   :math:`\sigma_8` is used as an indirect parametrisation for the primordial power spectrum scalar amplitude :math:`A_s`.
   For a given :math:`A_s`, the emulator must be given the corresponding :math:`\sigma_8` assuming a :math:`\Lambda\mathrm{CDM}` linear evolution, and not a :math:`f(R)\mathrm{CDM}` one.

The allowed parameter range is:

+-----------------------------------------------+-------------+-------------+
| :math:`-\log{|f_{R_0}|}`                      | 4           | 7           |
+-----------------------------------------------+-------------+-------------+
| :math:`\Omega_m`                              | 0.2365      | 0.3941      |
+-----------------------------------------------+-------------+-------------+
| :math:`\sigma_8`                              | 0.6083      | 1.0140      |
+-----------------------------------------------+-------------+-------------+
| :math:`a_\mathrm{exp}`                        | 0.3333      | 1           |
+-----------------------------------------------+-------------+-------------+
| :math:`k \ \left[h\mathrm{Mpc}^{-1}\right]`   | 0.03        | 10          |
+-----------------------------------------------+-------------+-------------+

For any parameter outside this emulation range, e-MANTIS will throw an error without returning any predictions.
In the case of the wavenumber :math:`k`, it is possible to safely extrapolate the predictions to lower values.
See the detailed API for more details.

The influence of the other standard cosmological parameters on the boost has been checked to be smaller than :math:`1\%` in the range :math:`0.5052 < h < 0.8420`, :math:`0.03698 < \Omega_b < 0.06162` and :math:`0.7237 < n_s < 1.206` (see paper).

Usage and examples
~~~~~~~~~~~~~~~~~~

.. toctree::

   tutorials/fofr_matter_power_boost_tutorial.ipynb
   fofr_matter_power_boost_api.rst
