.. _hmf:

Halo mass function in :math:`w\mathrm{CDM}` and :math:`f(R)` gravity
--------------------------------------------------------------------

e-MANTIS implements an emulator for the halo mass function (HMF) supporting different cosmological models and dark matter halo definitions.
This emulator is based on a dedicated suite of :math:`N`-body simulations, the extended e-MANTIS simulation suite presented in ???.
Two types of cosmological models are supported:

#. :math:`w\mathrm{CDM}`: dark energy with constant equation of state parameter :math:`w`,
#. :math:`f(R)` modified gravity model using the Hu & Sawicki model (limited to :math:`n=1`),

for two types of dark matter haloes:

#. Friend-of-Friends (FoF) haloes, using a linking length parameter :math:`b=0.2`,
#. Spherical Overdensity (SO) haloes, for multiple values of the critical overdensity threshold: :math:`\Delta_\mathrm{c}=200,500,1000`.

More details about the simulations used to obtain the training data, emulation procedure and the estimated accuracy can be found in `The e-MANTIS emulator: fast and accurate predictions of the halo mass function in f(R)CDM and wCDM cosmologies <https://arxiv.org/pdf/2410.05226>`_.
If you use this emulator, please cite this paper.

Emulation range
~~~~~~~~~~~~~~~

.. warning::

   We follow the standard convention of the field, and by :math:`\sigma_8` we refer to the present-day root-mean-square linear matter fluctuation averaged over a sphere of radius :math:`8h^{-1}\mathrm{Mpc}`, but assuming general relativity.
   In the case of :math:`w\mathrm{CDM}` this corresponds to the usual :math:`\sigma_8`.
   However, in the case of :math:`f(R)` gravity, it corresponds to the :math:`\sigma_8` in a :math:`\Lambda\mathrm{CDM}` cosmology with identical cosmological parameters other than :math:`f_{R_0}`.

The allowed parameter range for the common standard cosmological parameters and scale factor is:

+-----------------------------------------------+-------------+-------------+
| :math:`\Omega_\mathrm{m}`                     | 0.155       | 0.465       |
+-----------------------------------------------+-------------+-------------+
| :math:`\sigma_8`                              | 0.6083      | 1.014       |
+-----------------------------------------------+-------------+-------------+
| :math:`h`                                     | 0.55        | 0.85        |
+-----------------------------------------------+-------------+-------------+
| :math:`n_\mathrm{s}`                          | 0.72        | 1.2         |
+-----------------------------------------------+-------------+-------------+
| :math:`\Omega_\mathrm{b}`                     | 0.037       | 0.062       |
+-----------------------------------------------+-------------+-------------+
| :math:`a_\mathrm{exp}`                        | 0.3333      | 1           |
+-----------------------------------------------+-------------+-------------+

For the :math:`w\mathrm{CDM}` model, the allowed range for the dark energy equation of state parameter is:

+-----------------------------------------------+-------------+-------------+
| :math:`w`                                     | -1.5        | -0.5        |
+-----------------------------------------------+-------------+-------------+

For :math:`f(R)` gravity, the allowed range of the :math:`f_{R_0}` parameter is:

+-----------------------------------------------+-------------+-------------+
| :math:`-\log_{10}{|f_{R_0}|}`                 | 4           | 7           |
+-----------------------------------------------+-------------+-------------+

For any parameter outside this emulation range, e-MANTIS will throw an exception without returning any predictions.
In terms of halo mass, the allowed range depends on the type of cosmological model, the halo definition, and the scale factor.
See the dedicated tutorial below for more details.

Usage and examples
~~~~~~~~~~~~~~~~~~

.. toctree::

   tutorials/halo_mass_function_tutorial.ipynb
   halo_mass_function_api.rst
