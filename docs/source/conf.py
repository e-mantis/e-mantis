# Configuration file for the Sphinx documentation builder.

# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.

import os
import sys

sys.path.insert(0, os.path.abspath("../../src/"))

# -- Project information -----------------------------------------------------

project = "e-mantis"
copyright = "2023, Iñigo Sáez-Casares - Université Paris Cité"
author = "Iñigo Sáez-Casares"
version = "1.1.0"
release = "1.1.0"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.napoleon",
    "sphinx.ext.autodoc",
    "sphinx.ext.mathjax",
    "sphinx.ext.coverage",
    "sphinx_copybutton",
    "myst_nb",
]
# "myst_parser" not needed if myst_nb is included.

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

# -- Autodoc configuration ---------------------------------------------------

# Change autodoc sorting order.
# autodoc_member_order = "groupwise"
autodoc_member_order = "bysource"

# -- MyST-NB configuration ---------------------------------------------------

myst_enable_extensions = [
    "amsmath",
    "colon_fence",
    "deflist",
    "dollarmath",
    "html_image",
]

nb_execution_timeout = 360

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.

# html_theme = "alabaster"
# html_theme = "sphinx_book_theme"  # needs to be installed with pip (Jupyter Book theme)
html_theme = "sphinx_rtd_theme"  # needs to be installed with pip (Read the Docs theme)
# TODO move to jupyter book theme, looks nicer be seems to require a bit more of customisation.

html_theme_options = {"collapse_navigation": False}  # for rtd

# html_theme_options = {"repository_url":
#                       "https://gitlab.obspm.fr/e-mantis/e-mantis",
#                       "repository_branch": True,
#                       "use_repository_button": True,
#                       "use_issues_button": True,
#                       "show_navbar_depth": <2>
#                       } # for jupyter book
