Detailed API
~~~~~~~~~~~~

.. autoclass:: emantis.halo_mass_function.HMFEmulator
    :members:
    :inherited-members:
