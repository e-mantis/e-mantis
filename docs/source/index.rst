.. e-mantis documentation master file, created by
   sphinx-quickstart on Thu Feb  2 15:52:54 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the documentation of e-MANTIS!
=========================================

e-MANTIS is the Emulator for Multiple observable ANalysis in extended cosmological TheorIeS.
It is a cosmological emulator providing fast and accurate theoretical predictions in the non-linear regime of structure formation.
The predictions are calibrated using the outputs from cosmological :math:`N`-body simulations.

The emulator supports multiple cosmological models and observables.
It is divided in multiple modules, each one focusing on a particular type of observable.
This project is under constant development and more things will progressively be added in the future.

.. _installation:

Installation
============

You can install the emulator from `PyPI <https://pypi.org/project/emantis/>`_ via pip::

  pip install emantis

Or you can directly clone the emulator from our public `repository <https://gitlab.obspm.fr/e-mantis/e-mantis>`_ and install it from source::

  git clone https://gitlab.obspm.fr/e-mantis/e-mantis.git
  cd e-mantis
  pip install [-e] .

The emulator only works with python >= 3.9. The main dependencies are:

* h5py (tested with version >= 3.8)
* scikit-learn (tested with version >= 1.0)
* numpy (tested with version >= 1.24)
* pydantic (tested with version >= 2.6)
* joblib (tested with version >= 1.3.2)
* tomli (tested with version >= 2.0, required only for python < 3.11)

All the dependencies should be installed automatically by pip.

.. _user_guide_toc:

Emulators
=========

The currently available emulators are:

.. toctree::
   :maxdepth: 1

   fofr_matter_power_boost_index
   halo_mass_function_index

.. Keep this in order to enable MathJax in the TOC tree.
   If no math directives are present in a given rst file, Sphinx does not activate MathJax in order to save loading time.
   Because of this, math expressions are not rendered in the TOC tree.
   Adding this dummy math expression solves the problem, even if it is not a very elegant solution.
   See https://github.com/sagemath/sage/issues/33347 and https://github.com/sphinx-doc/sphinx/issues/10192.
.. math::
   :nowrap:
