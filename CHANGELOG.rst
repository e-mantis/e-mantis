Changelog
#########

Unreleased
==========

.. _release-1.1.0:

v1.1.0 (07/10/2024)
===================

New features
------------

* New emulator for the halo mass function in f(R)CDM and wCDM cosmologies.
* New generic module to handle emulation with Gaussian processes.

.. _release-1.0.4:

v1.0.4 (27/09/2023)
===================

New features
------------

* The emulator can now be trained using multiple processes thanks to scikit-learn's ``MultiOutputRegressor``.
* The emulated boost can now be extrapolated to smaller scale factors outside the emulation range (deactivated by default).
* It is now possible to request predictions for the boost at specific wavenumbers (linear interpolation in log10(k)).
* The emulated boost can now be extrapolated for wavenumbers below the emulation range (deactivated by default).

.. _release-1.0.3:

v1.0.3 (19/04/2023)
===================

Fixes
-----

* Fixed bug introduced in v1.0.2 by the multiple scale factors feature.
  This bug caused the emulator to fail when requesting predictions for scale factors outside the training nodes.

.. _release-1.0.2:

v1.0.2
======

* This is the first official public release.

New features
------------

* The emulator is now able to make predictions for multiple scale factors per cosmological model with a single call.

Changes
-------

* Data files have been moved to a separate subfolder inside the package.
* Data files are now loaded using ``importlib.resources`` without manipulating the package's ``__file__`` attribute.
* Suppressed scikit-learn ``ConvergenceWarning`` warnings during emulator training (safe).

.. _release-1.0.1:

v1.0.1
======

Fixes
-----

* Fixed pip packaging.

.. _release-1.0.0:

v1.0.0
======

* Initial version.
